import React from 'react'
import './App.css'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <strong>try to follow the rules:</strong>
        <ul>
          <li>1. create UI components in /components</li>
          <li>2. try to seperate logic from UI</li>
          <li>3. commit in small chunks of code so we can follow your ideas and progress</li>
          <li>4. write clean and readable code</li>
          <li>5. keep things as simple as possible</li>
          <li>6. put comments in code when you want to explain or discuss something</li>
        </ul>
      </header>
      <h3>start creating your spreadsheet here</h3>
    </div>
  )
}

export default App
